# Develop Think

## Map

we define serveral maps in 'assets/Map/maps', each map saved several tiles,
each tile saved it's position, house info is also saved in the tiles, including house sell price, update price and id;

## save map info

the house info is saved in 'houseMap', the houseMap is a ES6 structure Map,
the key is the id of the house, it also includes some other info, for example owner, current level,
how much to pay when passing and other info so on;

## save user info

I define a Map in DataManager.Instance, the map saves all player's info including money, status, houses and balabala

## two people's game order

SceneManager plays the role of director, it determain which player to go
