import { _decorator, Component, Button, Label } from 'cc'
import DataManager from '../../Runtime/DataManager'
import EventManager from '../../Runtime/EventManager'
import { EventType } from '../../Enums'
import { chanceMap } from '../../Map/maps/China'
const { ccclass, property } = _decorator

@ccclass('ChanceController')
export class ChanceController extends Component {
    @property(Button)
    okBtn: Button

    @property(Label)
    label: Label

    protected onLoad(): void {
        this.bindEventListener()
    }
    bindEventListener(): void {
        this.okBtn && this.okBtn.node?.on('click', this.handleOKClicked)
    }
    handleOKClicked() {
        EventManager.Instance.emit(EventType.CHOOSE_CHANCE)
    }
    updateUI() {
        const chanceId = DataManager.Instance.getCurrentChanceId()
        const _chance = chanceMap.get(chanceId)
        this.label.string = _chance.desc
    }
}
