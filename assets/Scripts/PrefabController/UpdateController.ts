import { _decorator, Component, Button, Label, Sprite } from 'cc'
import DataManager from '../../Runtime/DataManager'
import EventManager from '../../Runtime/EventManager'
import { EventType } from '../../Enums'
import { IHouse } from '../../Type/house'
import ResourceManager from '../../Runtime/ResourceManager'
const { ccclass, property } = _decorator

@ccclass('UpdateController')
export class UpdateController extends Component {
    @property(Button)
    updateBtn: Button

    @property(Button)
    refuseBtn: Button

    @property(Label)
    positionLabel: Label

    @property(Label)
    houseNameLabel: Label

    @property(Label)
    houseLevelLabel: Label

    @property(Label)
    updateHouseLabel: Label

    @property(Label)
    tollLabel: Label

    @property(Sprite)
    houseImgSprite: Sprite

    protected onLoad(): void {
        this.bindEventListener()
    }
    bindEventListener(): void {
        this.updateBtn && this.updateBtn.node?.on('click', this.handleUpdateClicked)
        this.refuseBtn && this.refuseBtn.node?.on('click', this.handleOKClicked)
    }
    handleUpdateClicked() {
        EventManager.Instance.emit(EventType.UPDATE_HOUSE)
    }
    handleOKClicked() {
        EventManager.Instance.emit(EventType.GO_NEXT)
    }
    // update UI
    updateUI() {
        // 更新各种Label
        const currentPlayer = DataManager.Instance.getCurrentPlayer()
        const houseInfo: IHouse = DataManager.Instance.getCurrentHouseHouseInfo()
        this.positionLabel.string = `${currentPlayer.name}, 您当前位于${houseInfo.title}`
        this.houseNameLabel.string = `房子名称: ${houseInfo.title}`
        const houseLevel = DataManager.Instance.houseOwnerMap.get(houseInfo.id).level
        this.houseLevelLabel.string = `当前等级: ${houseLevel} 级`
        this.updateHouseLabel.string = `升级费用: ${houseInfo.updateMoney}`
        const levelKey = `level${houseLevel}`
        this.tollLabel.string = `过路费: ${houseInfo.roadToll[levelKey]}`

        // 更新房子等级UI
        this.houseImgSprite.spriteFrame = ResourceManager.Instance.getResource(levelKey, 'house')
    }
}
