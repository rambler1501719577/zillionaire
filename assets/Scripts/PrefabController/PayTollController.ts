import { _decorator, Component, Button, Label } from 'cc'
import DataManager from '../../Runtime/DataManager'
import EventManager from '../../Runtime/EventManager'
import { EventType } from '../../Enums'
const { ccclass, property } = _decorator

@ccclass('PayTollController')
export class PayTollController extends Component {
    @property(Button)
    okBtn: Button

    @property(Label)
    descLabel: Label

    protected onLoad(): void {
        this.bindEventListener()
    }
    bindEventListener(): void {
        this.okBtn && this.okBtn.node?.on('click', this.handleOKClicked)
    }
    handleOKClicked() {
        EventManager.Instance.emit(EventType.PAY_TOLL)
    }
    // update UI
    updateUI() {
        const currentPlayer = DataManager.Instance.getCurrentPlayer()
        const houseId = DataManager.Instance.getCurrentHouseId()
        const houseInfo = DataManager.Instance.houseMap.get(houseId)
        const owner = DataManager.Instance.getHouseOwner(houseId)
        const level = `level${owner.level}`
        const toll = houseInfo.roadToll[level]
        this.descLabel.string = `${currentPlayer.name}的房屋等级为${owner.level}级，需要交过路费${toll}`
    }
}
