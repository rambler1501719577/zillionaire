import { _decorator, Component, Button, Label } from 'cc'
import DataManager from '../../Runtime/DataManager'
import EventManager from '../../Runtime/EventManager'
import { EventType } from '../../Enums'
import { IHouse } from '../../Type/house'
const { ccclass, property } = _decorator

@ccclass('PurchaseController')
export class PurchaseController extends Component {
    @property(Button)
    purchaseBtn: Button

    @property(Button)
    refuseBtn: Button

    @property(Label)
    positionLabel: Label

    @property(Label)
    houseNameLabel: Label

    @property(Label)
    purchaseHouseLabel: Label

    @property(Label)
    updateHouseLabel: Label

    @property(Label)
    tollLabel: Label

    protected onLoad(): void {
        this.bindEventListener()
    }
    bindEventListener(): void {
        this.purchaseBtn && this.purchaseBtn.node?.on('click', this.handlePurchaseClicked)
        this.refuseBtn && this.refuseBtn.node?.on('click', this.handleOKClicked)
    }
    handlePurchaseClicked() {
        EventManager.Instance.emit(EventType.BUY_HOUSE)
    }
    handleOKClicked() {
        EventManager.Instance.emit(EventType.GO_NEXT)
    }
    // update UI
    updateUI() {
        const currentPlayer = DataManager.Instance.getCurrentPlayer()
        const houseInfo: IHouse = DataManager.Instance.getCurrentHouseHouseInfo()
        this.positionLabel.string = `${currentPlayer.name}, 您当前位于${houseInfo.title}`
        this.houseNameLabel.string = `房产名称: ${houseInfo.title}`
        this.purchaseHouseLabel.string = `购买费用: ${houseInfo.sellMoney}`
        this.updateHouseLabel.string = `升级费用: ${houseInfo.updateMoney}`
        this.tollLabel.string = `过路费:
        空地: ${houseInfo.roadToll.level0}
        一级房屋: ${houseInfo.roadToll.level1}
        二级房屋: ${houseInfo.roadToll.level2}
        顶级房屋: ${houseInfo.roadToll.level3}`
    }
}
