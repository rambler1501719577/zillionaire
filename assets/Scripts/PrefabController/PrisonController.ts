import { _decorator, Component, Button, Label } from 'cc'
import DataManager from '../../Runtime/DataManager'
import EventManager from '../../Runtime/EventManager'
import { EventType } from '../../Enums'
const { ccclass, property } = _decorator

@ccclass('PrisonController')
export class PrisonController extends Component {
    @property(Button)
    okBtn: Button

    protected onLoad(): void {
        this.bindEventListener()
    }
    bindEventListener(): void {
        this.okBtn && this.okBtn.node?.on('click', this.handleOKClicked)
    }
    handleOKClicked() {
        EventManager.Instance.emit(EventType.PAY_FOR_PRISON)
    }
}
