import { _decorator, Component, Button, Label } from 'cc'
import DataManager from '../../Runtime/DataManager'
import EventManager from '../../Runtime/EventManager'
import { EventType } from '../../Enums'
import { destinyMap } from '../../Map/maps/China'
const { ccclass, property } = _decorator

@ccclass('DestinyController')
export class DestinyController extends Component {
    @property(Button)
    okBtn: Button

    @property(Label)
    label: Label

    protected onLoad(): void {
        this.bindEventListener()
    }
    bindEventListener(): void {
        this.okBtn && this.okBtn.node?.on('click', this.handleOKClicked)
    }
    handleOKClicked() {
        EventManager.Instance.emit(EventType.CHOOSE_DESTINY)
    }
    updateUI() {
        const destinyId = DataManager.Instance.getCurrentDestinyId()
        const destiny = destinyMap.get(destinyId)
        this.label.string = destiny.desc
    }
}
