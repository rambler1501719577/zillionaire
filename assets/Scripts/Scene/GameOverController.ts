import { _decorator, Component, Label, director, Node } from 'cc'
import DataManager from '../../Runtime/DataManager'
const { ccclass, property } = _decorator

@ccclass('PassHouseController')
export class PassHouseController extends Component {
    @property(Label)
    label: Label
    protected onLoad(): void {
        this.label.string = `Loser is 【${DataManager.Instance.loser}】`
    }
}
