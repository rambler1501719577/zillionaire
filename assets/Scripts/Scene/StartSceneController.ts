import { _decorator, Component, Node, director, Button, Label, UITransform, EventHandler } from 'cc'
const { ccclass, property } = _decorator

@ccclass('StartSceneController')
export class StartSceneController extends Component {
    @property(Button)
    startButton: Button

    start() {}
    protected onLoad(): void {
        this.startButton.node.on('click', this.callback, this)
    }

    callback(event: Event) {
        director.loadScene('Main')
    }
}
