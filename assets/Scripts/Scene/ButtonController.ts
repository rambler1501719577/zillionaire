import {
    _decorator,
    Component,
    Node,
    EventHandler,
    Button,
    Label,
    director,
    Sprite,
    UITransform,
    resources,
    SpriteFrame,
} from 'cc'
const { ccclass, property } = _decorator
import { createUINode, randomNum } from '../../Utils/index'
import DataManager from '../../Runtime/DataManager'
import { IHouse } from '../../Type/house'
import EventManager from '../../Runtime/EventManager'
import { EventType } from '../../Enums'

@ccclass('ButtonController')
export class NewComponent extends Component {
    @property(Label)
    label: Label

    houseMap: Map<number, IHouse>
    position: number = 0

    protected onLoad(): void {
        const button = this.node.getComponent(Button)
        button.node.on('click', () => {
            if (DataManager.Instance.enableNextTick) {
                EventManager.Instance.emit(EventType.NEXT_TICK)
            }
        })
    }
}
