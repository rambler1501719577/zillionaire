import { _decorator, Component, Label, Layers, Node, resources, Sprite, SpriteFrame, UITransform } from 'cc'
import DataManager from '../../Runtime/DataManager'
const { ccclass, property } = _decorator
import { house } from '../../Map/maps/China'
import { IHouse } from '../../Type/house'
import { ITile } from '../../Map'
import { createUINode } from '../../Utils'
@ccclass('TileManager')
export class TileManager extends Component {
    init(spriteFrame: SpriteFrame, i: number, j: number, blockInfo: ITile) {
        const { blockWidth, blockHeight } = DataManager.Instance
        // 本身就是一个node类型
        const sprite = this.addComponent(Sprite)
        sprite.spriteFrame = spriteFrame

        // label组件注册
        const houseInfo: IHouse | undefined = house.get(blockInfo.houseId)
        if (houseInfo && houseInfo.title) {
            const labelNode = this.createLabelNode(houseInfo)
            labelNode.setPosition(blockWidth / 2, -blockHeight / 2)
            labelNode.setParent(this.node)
        }

        // transform 组件

        const uiTransform = this.getComponent(UITransform)
        uiTransform.setContentSize(blockWidth, blockHeight)
        this.node.setPosition(j * blockWidth, -i * blockHeight)
    }
    createLabelNode(houseInfo: IHouse): Node {
        const labelNode = createUINode()
        const label = labelNode.addComponent(Label)
        // justify ui
        const uiTransform = label.getComponent(UITransform)
        uiTransform.setAnchorPoint(0.5, 0.5)

        label.string = houseInfo.title
        label.fontSize = 16
        return labelNode
    }
}
