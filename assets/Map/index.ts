import China from './maps/China'
import World from './maps/world/World'
import { IHouse } from '../Type/house'
import { TileType } from '../Type/game'

export interface ITile {
    src: TileType | null
    type: TileType
    houseId?: number
    chanceId?: number
    destinyId?: number
}

export interface IMap {
    mapInfo: Array<Array<ITile>> // 地图图块信息
    house: Map<number, IHouse> // 房产信息
}

const maps: Record<string, IMap> = {
    China,
    World,
}

export default maps
