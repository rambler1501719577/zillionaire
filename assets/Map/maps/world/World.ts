import { IMap, ITile } from '../../index'
import { IHouse } from '../../../Type/house'
import { IChance, IDestiny, TileType } from '../../../Type/game'

const map: Array<Array<ITile>> = [
    [
        { src: null, type: null },
        { src: null, type: null },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: null, type: null },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: null, type: null },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: null, type: TileType.house, houseId: 13 },
    ],
    [
        { src: null, type: null },
        { src: TileType.start, type: TileType.start },
        { src: TileType.house, type: TileType.house, houseId: 1 },
        { src: TileType.house, type: TileType.house, houseId: 2 },
        { src: TileType.house, type: TileType.house, houseId: 3 },
        { src: TileType.chance, type: TileType.chance, chanceId: 1 },
        { src: TileType.house, type: TileType.house, houseId: 4 },
        { src: TileType.house, type: TileType.house, houseId: 5 },
        { src: TileType.house, type: TileType.house, houseId: 6 },
        { src: TileType.house, type: TileType.house, houseId: 7 },
        { src: TileType.house, type: TileType.house, houseId: 8 },
        { src: TileType.house, type: TileType.house, houseId: 9 },
        { src: TileType.destiny, type: TileType.destiny, destinyId: 1 },
        { src: TileType.house, type: TileType.house, houseId: 10 },
        { src: TileType.house, type: TileType.house, houseId: 11 },
        { src: TileType.house, type: TileType.house, houseId: 12 },
        { src: TileType.house, type: TileType.house, houseId: 13 },
        { src: null, type: null },
    ],
    [
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.house, type: TileType.house, houseId: 14 },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: TileType.house, type: TileType.house, houseId: 15 },
        { src: TileType.houseSign, type: TileType.houseSign },
    ],
    [
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.house, type: TileType.house, houseId: 16 },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: TileType.chance, type: TileType.chance, chanceId: 2 },
        { src: null, type: null },
    ],
    [
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.house, type: TileType.house, houseId: 17 },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: TileType.house, type: TileType.house, houseId: 22 },
        { src: TileType.houseSign, type: TileType.houseSign },
    ],
    [
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.house, type: TileType.house, houseId: 23 },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: TileType.house, type: TileType.house, houseId: 26 },
        { src: TileType.houseSign, type: TileType.houseSign },
    ],
    [
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.house, type: TileType.house, houseId: 27 },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: null, type: null },
        { src: TileType.house, type: TileType.house, houseId: 30 },
        { src: TileType.houseSign, type: TileType.houseSign },
    ],
    [
        { src: null, type: null },
        { src: TileType.prison, type: TileType.prison },
        { src: TileType.house, type: TileType.house, houseId: 31 },
        { src: TileType.house, type: TileType.house, houseId: 32 },
        { src: TileType.house, type: TileType.house, houseId: 33 },
        { src: TileType.house, type: TileType.house, houseId: 34 },
        { src: TileType.chance, type: TileType.chance, chanceId: 3 },
        { src: TileType.house, type: TileType.house, houseId: 36 },
        { src: TileType.house, type: TileType.house, houseId: 37 },
        { src: TileType.house, type: TileType.house, houseId: 38 },
        { src: TileType.house, type: TileType.house, houseId: 38 },
        { src: TileType.house, type: TileType.house, houseId: 38 },
        { src: TileType.house, type: TileType.house, houseId: 38 },
        { src: TileType.house, type: TileType.house, houseId: 39 },
        { src: TileType.house, type: TileType.house, houseId: 40 },
        { src: TileType.house, type: TileType.house, houseId: 41 },
        { src: TileType.destiny, type: TileType.destiny, destinyId: 3 },
        { src: null, type: null },
    ],
    [
        { src: null, type: null },
        { src: null, type: null },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: null, type: null },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: TileType.houseSign, type: TileType.houseSign },
        { src: null, type: null },
        { src: null, type: TileType.house, houseId: 13 },
    ],
]
export const house = new Map<number, IHouse>()
const houses = [
    {
        id: 1,
        title: '美国',
        sellMoney: 2123,
        updateMoney: 1688,
        roadToll: {
            level0: 265,
            level1: 507,
            level2: 1637,
            level3: 5568,
        },
    },
    {
        id: 2,
        title: '俄罗斯',
        sellMoney: 3941,
        updateMoney: 1583,
        roadToll: {
            level0: 364,
            level1: 795,
            level2: 1841,
            level3: 5180,
        },
    },
    {
        id: 3,
        title: '英国',
        sellMoney: 2945,
        updateMoney: 536,
        roadToll: {
            level0: 94,
            level1: 901,
            level2: 1498,
            level3: 8484,
        },
    },
    {
        id: 4,
        title: '法国',
        sellMoney: 3964,
        updateMoney: 1453,
        roadToll: {
            level0: 93,
            level1: 695,
            level2: 1663,
            level3: 8663,
        },
    },
    {
        id: 5,
        title: '德国',
        sellMoney: 1233,
        updateMoney: 1863,
        roadToll: {
            level0: 168,
            level1: 552,
            level2: 1493,
            level3: 7422,
        },
    },
    {
        id: 6,
        title: '日本',
        sellMoney: 3876,
        updateMoney: 2310,
        roadToll: {
            level0: 108,
            level1: 875,
            level2: 1366,
            level3: 6249,
        },
    },
    {
        id: 7,
        title: '加拿大',
        sellMoney: 2258,
        updateMoney: 2733,
        roadToll: {
            level0: 396,
            level1: 783,
            level2: 1398,
            level3: 5423,
        },
    },
    {
        id: 8,
        title: '巴西',
        sellMoney: 3019,
        updateMoney: 2093,
        roadToll: {
            level0: 345,
            level1: 868,
            level2: 1581,
            level3: 8644,
        },
    },
    {
        id: 9,
        title: '墨西哥',
        sellMoney: 3427,
        updateMoney: 1166,
        roadToll: {
            level0: 64,
            level1: 857,
            level2: 1561,
            level3: 8035,
        },
    },
    {
        id: 10,
        title: '阿根廷',
        sellMoney: 2428,
        updateMoney: 2926,
        roadToll: {
            level0: 498,
            level1: 530,
            level2: 1500,
            level3: 6317,
        },
    },
    {
        id: 11,
        title: '意大利',
        sellMoney: 3805,
        updateMoney: 1758,
        roadToll: {
            level0: 55,
            level1: 771,
            level2: 1423,
            level3: 7732,
        },
    },
    {
        id: 12,
        title: '印度',
        sellMoney: 1496,
        updateMoney: 1577,
        roadToll: {
            level0: 412,
            level1: 924,
            level2: 1772,
            level3: 5689,
        },
    },
    {
        id: 13,
        title: '韩国',
        sellMoney: 1252,
        updateMoney: 735,
        roadToll: {
            level0: 41,
            level1: 831,
            level2: 1572,
            level3: 8503,
        },
    },
    {
        id: 14,
        title: '西班牙',
        sellMoney: 1471,
        updateMoney: 1562,
        roadToll: {
            level0: 248,
            level1: 950,
            level2: 1207,
            level3: 5056,
        },
    },
    {
        id: 15,
        title: '澳大利亚',
        sellMoney: 3889,
        updateMoney: 1747,
        roadToll: {
            level0: 235,
            level1: 898,
            level2: 1108,
            level3: 6348,
        },
    },
    {
        id: 16,
        title: '荷兰',
        sellMoney: 3487,
        updateMoney: 1345,
        roadToll: {
            level0: 388,
            level1: 840,
            level2: 1757,
            level3: 6038,
        },
    },
    {
        id: 17,
        title: '瑞典',
        sellMoney: 2295,
        updateMoney: 988,
        roadToll: {
            level0: 217,
            level1: 591,
            level2: 1619,
            level3: 7407,
        },
    },
    {
        id: 18,
        title: '比利时',
        sellMoney: 2059,
        updateMoney: 2716,
        roadToll: {
            level0: 93,
            level1: 720,
            level2: 1009,
            level3: 8621,
        },
    },
    {
        id: 19,
        title: '丹麦',
        sellMoney: 1399,
        updateMoney: 2988,
        roadToll: {
            level0: 289,
            level1: 740,
            level2: 1053,
            level3: 6581,
        },
    },
    {
        id: 20,
        title: '芬兰',
        sellMoney: 1885,
        updateMoney: 1573,
        roadToll: {
            level0: 49,
            level1: 645,
            level2: 1369,
            level3: 7211,
        },
    },
    {
        id: 21,
        title: '瑞士',
        sellMoney: 1719,
        updateMoney: 541,
        roadToll: {
            level0: 142,
            level1: 854,
            level2: 1188,
            level3: 7033,
        },
    },
    {
        id: 22,
        title: '希腊',
        sellMoney: 2548,
        updateMoney: 2641,
        roadToll: {
            level0: 263,
            level1: 585,
            level2: 1255,
            level3: 5723,
        },
    },
    {
        id: 23,
        title: '挪威',
        sellMoney: 3511,
        updateMoney: 2498,
        roadToll: {
            level0: 469,
            level1: 688,
            level2: 1393,
            level3: 5091,
        },
    },
    {
        id: 24,
        title: '奥地利',
        sellMoney: 3262,
        updateMoney: 756,
        roadToll: {
            level0: 71,
            level1: 643,
            level2: 1529,
            level3: 7252,
        },
    },
    {
        id: 25,
        title: '葡萄牙',
        sellMoney: 3350,
        updateMoney: 1033,
        roadToll: {
            level0: 437,
            level1: 703,
            level2: 1795,
            level3: 5901,
        },
    },
    {
        id: 26,
        title: '波兰',
        sellMoney: 1992,
        updateMoney: 1051,
        roadToll: {
            level0: 256,
            level1: 729,
            level2: 1464,
            level3: 7373,
        },
    },
    {
        id: 27,
        title: '捷克',
        sellMoney: 2255,
        updateMoney: 1449,
        roadToll: {
            level0: 401,
            level1: 533,
            level2: 1427,
            level3: 8051,
        },
    },
    {
        id: 28,
        title: '匈牙利',
        sellMoney: 3031,
        updateMoney: 1464,
        roadToll: {
            level0: 70,
            level1: 707,
            level2: 1239,
            level3: 6163,
        },
    },
    {
        id: 29,
        title: '新加坡',
        sellMoney: 3947,
        updateMoney: 1725,
        roadToll: {
            level0: 74,
            level1: 642,
            level2: 1600,
            level3: 5961,
        },
    },
    {
        id: 30,
        title: '马来西亚',
        sellMoney: 1427,
        updateMoney: 2646,
        roadToll: {
            level0: 342,
            level1: 580,
            level2: 1657,
            level3: 7853,
        },
    },
    {
        id: 31,
        title: '泰国',
        sellMoney: 2239,
        updateMoney: 1368,
        roadToll: {
            level0: 84,
            level1: 587,
            level2: 1609,
            level3: 6475,
        },
    },
    {
        id: 32,
        title: '印尼',
        sellMoney: 3488,
        updateMoney: 2465,
        roadToll: {
            level0: 79,
            level1: 719,
            level2: 1764,
            level3: 7054,
        },
    },
    {
        id: 33,
        title: '沙特',
        sellMoney: 1462,
        updateMoney: 803,
        roadToll: {
            level0: 269,
            level1: 545,
            level2: 1504,
            level3: 5432,
        },
    },
    {
        id: 34,
        title: '阿联酋',
        sellMoney: 3196,
        updateMoney: 2545,
        roadToll: {
            level0: 462,
            level1: 888,
            level2: 1308,
            level3: 5286,
        },
    },
    {
        id: 35,
        title: '南非',
        sellMoney: 3203,
        updateMoney: 1720,
        roadToll: {
            level0: 368,
            level1: 745,
            level2: 1466,
            level3: 7910,
        },
    },
    {
        id: 36,
        title: '埃及',
        sellMoney: 2586,
        updateMoney: 1575,
        roadToll: {
            level0: 347,
            level1: 717,
            level2: 1597,
            level3: 6273,
        },
    },
    {
        id: 37,
        title: '摩洛哥',
        sellMoney: 2027,
        updateMoney: 1146,
        roadToll: {
            level0: 147,
            level1: 762,
            level2: 1400,
            level3: 5253,
        },
    },
    {
        id: 38,
        title: '尼日利亚',
        sellMoney: 1507,
        updateMoney: 1703,
        roadToll: {
            level0: 81,
            level1: 550,
            level2: 1167,
            level3: 8573,
        },
    },
    {
        id: 39,
        title: '加纳',
        sellMoney: 3988,
        updateMoney: 1933,
        roadToll: {
            level0: 129,
            level1: 950,
            level2: 1869,
            level3: 5510,
        },
    },
    {
        id: 40,
        title: '梵蒂冈',
        sellMoney: 1898,
        updateMoney: 1933,
        roadToll: {
            level0: 329,
            level1: 950,
            level2: 1869,
            level3: 5510,
        },
    },
    {
        id: 41,
        title: '中国',
        sellMoney: 2617,
        updateMoney: 2302,
        roadToll: {
            level0: 163,
            level1: 588,
            level2: 1086,
            level3: 5144,
        },
    },
]
houses.forEach(h => house.set(h.id, h))

const China: IMap = {
    mapInfo: map,
    house: house,
}

export const chanceMap = new Map<number, IChance>()
chanceMap.set(1, {
    desc: '从巴拿马偷渡去美国被抓, 罚款1000元',
    reward: -1000,
})
chanceMap.set(2, {
    desc: '到拉斯维加斯赌城赌博输光现金, 银行大发慈悲救助3000元',
    reward: 3000,
})
chanceMap.set(3, {
    desc: '弃商从文的你, 从0开始学编程, 半年没找到工作, 损失800',
    reward: -800,
})

export const destinyMap = new Map<number, IDestiny>()
destinyMap.set(1, {
    desc: '上班带薪拉屎, 隔壁是老板, 被抓包, 罚款3000',
    reward: -3000,
})
destinyMap.set(2, {
    desc: '在哥斯拉大战金刚时, 你奋勇而出, 驾驶核潜艇偷袭哥斯拉, 系统奖励5000',
    reward: 5000,
})
destinyMap.set(3, {
    desc: '扣你一千块需要理由吗???',
    reward: -1000,
})
export default China
