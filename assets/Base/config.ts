export const config = {
    maxUpdateLevel: 3,
    payForPassStart: 2000,
    stepDuration: 500,
    stepGap: 700, // 经过stepGap时间后移动结束在弹出窗口
    defaultPlayerCount: 2,
    prisonCost: 2000,
    initPlayerMoney: 20000,
}
