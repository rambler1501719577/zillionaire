export enum EWallType {}
export enum Scene {
    Main = 'Main',
    BuyHousePage = 'BuyHousePage',
    StartScene = 'StartScene',
}

export enum EventType {
    NEXT_TICK = 'next-tick',
    BUY_HOUSE = 'buy-house',
    PAY_TOLL = 'pay-toll',
    UPDATE_HOUSE = 'update-house',
    GO_NEXT = 'go-next',
    CHOOSE_CHANCE = 'choose-chance',
    CHOOSE_DESTINY = 'choose-destiny',
    PAY_FOR_PRISON = 'pay-for-prison',
}
