export interface IRoadToll {
    level0: number
    level1: number
    level2: number
    level3: number
}
export interface IHouse {
    id: number // 每个房产,
    title: string // 房产名称
    desc?: string // 简介
    sellMoney: number
    updateMoney: number
    roadToll: IRoadToll //过路费
}
