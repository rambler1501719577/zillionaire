export enum PlayerStatus {
    stop = 'STOP',
    idle = 'IDLE',
}

export interface IMapInfo {
    left: number
    top: number
    type: TileType
    houseId?: number
    chanceId?: number
    destinyId?: number
}

export interface IHouseOwner {
    player: string
    level: number
}

export enum TileType {
    houseSign = 'house-sign', //
    house = 'house',
    chance = 'chance',
    destiny = 'destiny',
    start = 'start',
    prison = 'prison',
}

export interface IChance {
    desc: string
    reward: number
}

export interface IDestiny {
    desc: string
    reward: number
}
