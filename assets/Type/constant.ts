export enum EPrefabName {
    purchase = 'Purchase',
    update = 'Update',
    toll = 'PayToll',
    chance = 'Chance',
    destiny = 'Destiny',
    prison = 'Prison',
}
