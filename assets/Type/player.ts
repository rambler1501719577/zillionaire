import { IHouse } from './house'
import { PlayerStatus } from './game'
/**
 * player info
 * each player have init money, status, houses and so on
 */
export interface IPlayer {
    name?: string
    houses: Array<string> // save house ids
    money: number
    status: PlayerStatus
    step: number // 当前步数
}
