import { resources, SpriteFrame } from 'cc'
import Singleton from '../Base/Singleton'
export default class ResourceManager extends Singleton {
    resourceMap: Map<string, any> = new Map()
    static get Instance() {
        return super.getInstance<ResourceManager>()
    }
    // 注册资源， 初始化
    // 根据名称获取资源
    getResource(path: string, dir: string) {
        const fullPath = `${dir}/${path}`
        if (this.resourceMap.has(fullPath)) {
            return this.resourceMap.get(fullPath)
        }
        return undefined
    }
    registeHouseSpriteFrames() {
        return new Promise((resolve, reject) => {
            resources.loadDir('house', SpriteFrame, (err, assets) => {
                if (assets.length > 0) {
                    assets.forEach((asset: SpriteFrame) => {
                        this.resourceMap.set(`house/${asset.name}`, asset)
                    })
                }
                resolve(true)
            })
        })
    }
    registePlayerSpriteFrames() {
        return new Promise((resolve, reject) => {
            resources.loadDir('player', SpriteFrame, (err, assets) => {
                if (assets.length > 0) {
                    assets.forEach((asset: SpriteFrame) => {
                        this.resourceMap.set(`player/${asset.name}`, asset)
                    })
                }
                resolve(true)
            })
        })
    }
}
