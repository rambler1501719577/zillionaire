import Singleton from '../Base/Singleton'
import { ITile } from '../Map'
import { IMapInfo } from '../Type/game'
import { IHouse } from '../Type/house'
import { IPlayer } from '../Type/player'
import { Node } from 'cc'
import { IHouseOwner } from '../Type/game'

export default class DataManager extends Singleton {
    static get Instance() {
        return super.getInstance<DataManager>()
    }
    // 地图相关数据
    mapInfo: Array<Array<ITile>>
    rowCount: number
    columnCount: number
    blockWidth: number = 70
    blockHeight: number = 70
    houseMap: Map<number, IHouse> = new Map()
    // 每块地图位置信息, 循环队列
    mapPositionQueue: Array<IMapInfo>
    // 玩家信息
    playerInfoMap: Map<string, IPlayer> = new Map()
    // 玩家node节点信息(有些api不熟悉, 先保存节点引用, 后续在根据api查询)
    playerNodeMap: Map<string, Node> = new Map()
    // players queue, for game order
    playerQueue: Array<string> = []
    // house belong info
    houseOwnerMap: Map<number, IHouseOwner> = new Map()
    gameOrder: number = 0
    // 游戏锁, 在弹窗时锁住按钮
    enableNextTick: boolean = true
    loser: string = ''
    getCurrentPlayerPosition() {
        const currentPlayer = this.getCurrentPlayer()
        const houseId = this.mapPositionQueue[currentPlayer.step]?.houseId
        const houseInfo = houseId ? this.houseMap.get(houseId) : null
        return {
            step: currentPlayer.step,
            houseInfo,
        }
    }
    getCurrentChanceId() {
        const currentPlayer = this.getCurrentPlayer()
        return this.mapPositionQueue[currentPlayer.step]?.chanceId
    }
    getCurrentDestinyId() {
        const currentPlayer = this.getCurrentPlayer()
        return this.mapPositionQueue[currentPlayer.step]?.destinyId
    }
    getCurrentHouseId() {
        const currentPlayer = this.getCurrentPlayer()
        return this.mapPositionQueue[currentPlayer.step]?.houseId
    }
    getCurrentHouseHouseInfo() {
        const houseId = this.getCurrentHouseId()
        return this.houseMap.get(houseId)
    }
    getHouseOwner(houseId: number): IHouseOwner {
        if (this.houseOwnerMap.has(houseId)) {
            return this.houseOwnerMap.get(houseId)
        }
        return null
    }
    getCurrentUserStep(): number {
        const currentPlayer = this.getCurrentPlayer()
        return currentPlayer?.step
    }
    getCurrentPlayer() {
        const index = this.gameOrder % this.playerQueue.length
        const currentPlayerKey = this.playerQueue[index]
        return this.playerInfoMap.get(currentPlayerKey)
    }
    // forword by current player
    setStep(step: number = 0) {
        const currentPlayer = this.getCurrentPlayer()
        const _step = currentPlayer.step + step
        // 取余形成环形队列
        currentPlayer.step = _step % this.mapPositionQueue.length
    }
    getCurrentPlayerNode() {
        const index = this.gameOrder % this.playerQueue.length
        const currentPlayerKey = this.playerQueue[index]
        return this.playerNodeMap.get(currentPlayerKey)
    }
    lock() {
        this.enableNextTick = false
    }
    unlock() {
        this.enableNextTick = true
    }
}
