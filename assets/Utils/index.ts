import { Layers, Node, SpriteFrame, UITransform, resources } from 'cc'
export const createUINode = (name: string = '') => {
    const node = new Node(name)
    const transform = node.addComponent(UITransform)
    transform.setAnchorPoint(0, 1)
    node.layer = 1 << Layers.nameToLayer('UI_2D')
    return node
}

export function randomNum(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min
}

export function loadSpriteFrames(path: string): Promise<SpriteFrame[]> {
    return new Promise((resolve, reject) => {
        resources.loadDir(path, SpriteFrame, (err, assets) => {
            if (err) {
                reject(err)
                return
            }
            resolve(assets)
        })
    })
}
